// let t = new Date()
// console.log(t)
// console.log(t.getTime())
// console.log(t.getFullYear())
// console.log(t.getMonth())
// console.log(t.getDay())
// console.log(t.getDate())
// console.log(t.getHours())
// console.log(t.getUTCHours())

function show_ge_time(){
    let days = ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", 
                "ხუთშაბათი", "პარასკევი", "შაბათი"]
    let el = document.getElementById("show-time")
    let t =  new Date();
    el.innerHTML = "დღეს არის "+days[t.getDay()]
}

function show_f_ge_time(){
    let days = ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", 
                "ხუთშაბათი", "პარასკევი", "შაბათი"]
    let el = document.getElementById("show-f-time")
    let t =  new Date();
    let n_day = document.getElementById("num-of-day")
    let day_index = (t.getDay() + parseInt(n_day.value)) % 7 
    el.innerHTML = n_day.value+" დღის შემდეგ იქნება "+days[day_index]
}

let c = 0

function welcome(){
    c++
    alert("Welcome!!! "+c)
    if(c==3){
        clearInterval(s)
    }
}

// setTimeout(welcome, 3000)

let s = setInterval(welcome, 5000)

function stop_alert(){
    console.log(s)
    clearInterval(s);
}