function start(){ 
    k = setInterval(add_squares, 5000)
}

function add_squares(){ 
    var cont = document.querySelector("#field")
    var N = document.getElementById("N")
    N =  parseInt(N.value)
    console.log(N)
    cont.innerHTML = ""
    if(N>=1 && N<=10){       
        for(let i=0; i<N; i++){
            var sq = document.createElement("div")
            sq.classList.add("sq")
            sq.style.backgroundColor = get_random_color()
            cont.appendChild(sq)
        }
    }
}


function get_random_color(){
    const r =  Math.floor(Math.random()*256)
    const g =  Math.floor(Math.random()*256)
    const b =  Math.floor(Math.random()*256)
    return `rgb(${r}, ${g}, ${b})`
}
