function f_example_1(){
    document.write("Hello Levan!!!<br>")
}

f_example_1()
f_example_1()

function f_example_2(name){
    document.write("Hello "+name+"!!!<br>")
}

f_example_2("Saba")
f_example_2("Oto")

function f_example_3(name, lastname){
    document.write("<i>Hello "+name+" "+lastname+"!!!</i><br>")
}

f_example_3("Saba", "Razmadze")

function f_example_4(N){
    if(N>10){
        document.write("Graten than 10")
    }else{
        document.write("Less than 10")
    }
}

// f_example_4(16)

function f_example_5(N){
    if(N<10){
        document.write("Less than 10")
    }else if(N<100){
        document.write("Number is between 10 and 100")
    }else{
        document.write("Graten than 100") 
    }
}

f_example_5(70)
