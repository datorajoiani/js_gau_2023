let x1 = 23
var x2 = 24
x3 = 25
const x4 = 26

x1 = 43
x2 = 44
x3 = 45
// x4 = 46

function scope_ex_1(){
    console.log(x1);
}

// scope_ex_1()

function scope_ex_2(){
    if(5<9){
        let x = 9
        console.log(x)
    }

    console.log(x);
}

// scope_ex_2()

function scope_ex_3(){
    if(5<9){
        var y = 9
        console.log(y)
    }

    console.log(y);
}

// scope_ex_3()

function scope_ex_4(){
    console.log(y)
}

// scope_ex_4()

function scope_ex_5(){
    if(5<9){
        z = 12
        console.log(z)
    }

    console.log(z);
}

scope_ex_5()

function scope_ex_6(){
    console.log(z)
}

scope_ex_6()